<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => true,
                'role' => 'modifier',
            ],
            [
                'name' => 'Invitado',
                'email' => 'invitado@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => true,
                'role' => 'viewer',
            ],
            [
                'name' => 'Inactivo',
                'email' => 'inactivo@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => false,
                'role' => 'viewer',
            ],
        ]);
    }
}
