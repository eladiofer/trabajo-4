<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contenedor;
class ContenedorController extends Controller
{
    public function index(){
        $contenedores = Contenedor::all();
        return view('contenedor.index')->with('contenedores', $contenedores);
    }
    public function buscar(Request $request)
    {
        $identificador = $request->input('identificador');
        $contenedoresFiltrados = Contenedor::with('cargadorPorContenedor')
            ->where('identificador', 'LIKE', "%$identificador%")
            ->get();

        return response()->json($contenedoresFiltrados);
    }
    public function create(){
        if(auth()->user()->role == 'modifier'){
            return view('contenedor.create');
        }else{
            return redirect('/')->with('error', 'No tienes permisos para acceder a esta página');
        }
    }
    public function store(Request $request){
        $contenedor = new Contenedor();
        $contenedor->identificador = $request->input('identificador');
        $contenedor->toneladas_maximas = $request->input('toneladas');
        $contenedor->color = $request->input('color');
        $contenedor->activo = $request->input('activo');
        $contenedor->save();
        return redirect('/contenedor')->with('success', 'Contenedor creado correctamente');
    }
}
