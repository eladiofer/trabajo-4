<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargamento;
use App\Models\Contenedor;
class CargamentoController extends Controller
{
    public function index()
    {
        $cargamentos = Cargamento::all();
        return view('cargamento.index')->with('cargamentos', $cargamentos);
    }

    public function buscar(Request $request)
    {
        $carga = $request->input('carga');
        $cargamentosFiltrados = Cargamento::with('cargadorPorContenedor')
            ->where('nombre', 'LIKE', "%$carga%")
            ->get();

        return response()->json($cargamentosFiltrados);
    }
    public function create (){
        if(auth()->user()->role == 'modifier'){
            $contenedores = Contenedor::all();
            return view('cargamento.create')->with('contenedores', $contenedores);
        }else{
            return redirect('/')->with('error', 'No tienes permisos para acceder a esta página');
        }
    }
    public function store(Request $request){
        $cargamento = new Cargamento();
        $cargamento->nombre = $request->input('name');
        $cargamento->carga_controlada = $request->input('controlado');
        $cargamento->save();
        return redirect('/cargamento')->with('success', 'Cargamento creado correctamente');
    }
}
