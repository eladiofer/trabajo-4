<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CargadorPorContenedor;
use App\Models\Contenedor;
use App\Models\Cargamento;
class CargadorPorContenedorController extends Controller
{
    public function index()
    {
        $carga_contenedores = CargadorPorContenedor::all();
        return view('carga_contenedor.index')->with('carga_contenedores', $carga_contenedores);
    }
    public function create()
    {
        if(auth()->user()->role == 'modifier'){
            $contenedores = Contenedor::all();
            $cargamentos = Cargamento::all();
            return view('carga_contenedor.create')
            ->with('contenedores', $contenedores)
            ->with('cargamentos', $cargamentos);
        }else{
            return redirect('/')->with('error', 'No tienes permisos para acceder a esta página');
        }
    }
    public function store(Request $request)
    {
        $carga_contenedor = new CargadorPorContenedor();
        $carga_contenedor->contenedor_id = $request->input('contenedor');
        $carga_contenedor->cargamento_id = $request->input('cargamento');
        $carga_contenedor->toneladas = $request->input('cantidad');
        $carga_contenedor->activo = $request->input('activo');
        $carga_contenedor->origen = $request->input('origen');
        $carga_contenedor->destino = $request->input('destino');
        $carga_contenedor->save();
        return redirect('/carga_contenedor')->with('success', 'Carga contenedor creada correctamente');
    }

}