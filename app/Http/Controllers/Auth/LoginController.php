<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Str;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
    
        $credentials = $request->only('email', 'password');
    
        if (Auth::attempt($credentials)) {
            if (Auth::user()->active) {
                return redirect()->intended('/home');
            } else {
                Auth::logout();
                return back()->withErrors(['active' => 'Tu cuenta está inactiva.']);
            }
        }
    
        return back()->withErrors(['email' => 'Credenciales incorrectas']);
    }
    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleGithubCallback()
{
    try {
        $githubUser = Socialite::driver('github')->user();
    } catch (\Exception $e) {
        return redirect('/login')->withErrors(['error' => 'GitHub authentication failed.']);
    }

    // Comprueba si el usuario ya existe en tu base de datos o crea uno nuevo
    $existingUser = User::where('email', $githubUser->email)->first();

    if ($existingUser) {
        // Si el usuario ya existe, inicia sesión
        Auth::login($existingUser);
    } else {
        // Si no existe, crea un nuevo usuario en tu base de datos
        $newUser = new User();
        $newUser->name = $githubUser->name;
        $newUser->email = $githubUser->email;

        // Genera una contraseña aleatoria para el nuevo usuario
        $randomPassword = Str::random(16);
        $newUser->password = bcrypt($randomPassword);

        $newUser->save();

        // Inicia sesión con el nuevo usuario
        Auth::login($newUser);
    }

    // Redirige a la página deseada después del inicio de sesión
    return redirect('/home');
}
}
