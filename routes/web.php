<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContenedorController;
use App\Http\Controllers\CargamentoController;
use App\Http\Controllers\CargadorPorContenedorController;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Auth\LoginController;


Route::get('/', function () {
    return view('Home');
})->name('home');

Route::middleware(['auth'])->group(function () {
    //contenedor
    Route::get('/contenedor', [ContenedorController::class, 'index'])->name('contenedor.index');
    Route::get('/contenedor/create', [ContenedorController::class, 'create'])->name('contenedor.create');
    Route::post('/contenedor', [ContenedorController::class, 'store'])->name('contenedor.store');
    //carga
    Route::get('/cargamento', [CargamentoController::class, 'index'])->name('cargamento.index');
    Route::get('/cargamento/create', [CargamentoController::class, 'create'])->name('cargamento.create');
    Route::post('/cargamento', [CargamentoController::class, 'store'])->name('cargamento.store');
    //carga_contenedor
    Route::get('/carga_contenedor', [CargadorPorContenedorController::class, 'index'])->name('carga_contenedor.index');
    Route::get('/carga_contenedor/create', [CargadorPorContenedorController::class, 'create'])->name('carga_contenedor.create');
    Route::post('/carga_contenedor', [CargadorPorContenedorController::class, 'store'])->name('carga_contenedor.store');

    Route::get('/buscarContenedor', [ContenedorController::class, 'buscar'])->name('buscar');
    Route::get('/buscarCargamento', [CargamentoController::class, 'buscar'])->name('buscarCargamento');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/auth/github', [LoginController::class, 'redirectToGithub'])->name('github.login');
Route::get('/auth/github/callback', [LoginController::class, 'handleGithubCallback'])->name('github.callback');
