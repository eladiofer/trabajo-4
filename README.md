<p align="center">
  <b>Examen laravel</b>
</p>

<p align="center">
  <img src="https://img.shields.io/badge/Laravel-v8.x-orange.svg" alt="Laravel Version">
  <img src="https://img.shields.io/badge/PHP-v8.1%2B-blue.svg" alt="PHP Version">
</p>

## 🚀 Inicio rápido

Sigue estos pasos para configurar y ejecutar el proyecto en tu máquina local.

### Prerrequisitos

Asegúrate de tener instalados los siguientes elementos en tu sistema:

- [PHP](https://www.php.net/) (versión >= 8.1)
- [Composer](https://getcomposer.org/)
- [Node.js y npm](https://nodejs.org/)
- [MySQL](https://www.mysql.com/) o cualquier otro sistema de gestión de bases de datos
- Preferible [Laragon](https://laragon.org/)

### Configuración del entorno

1. **Clona el repositorio:**
   ```bash
   git clone https://gitlab.com/eladiofer/trabajo-4.git
   ```
2. **Instalar dependencias:**
    ```
    cd trabajo-4
    composer install
    ```
3.  **Copiar el .env:**
    ```
    cp .env.example .env
    ```
4. **Generar la clave de la aplicacion:**
    ```
    php artisan key:generate
    ```
5. **Ejecuta las migraciones y los seeders:**
    ```
    php artisan migrate --seed
    ```
6. **Copiamos las claves de github al .env:**
    ```
    GITHUB_CLIENT_ID= 22af13c20730f8e47a8e
    GITHUB_CLIENT_SECRET= 97fee6a9bf317f2627fc6197627a8b0f394ce667
    GITHUB_CALLBACK_URL="http://127.0.0.1/auth/github/callback"
    ```
7. **Levantamos el servidor:**
    ```
    php artisan serve
    ```

