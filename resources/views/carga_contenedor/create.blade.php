@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
@include('user-welcome')

            <a href="{{ route('home') }}" class="btn btn-secondary">Volver atrás</a>
        </div>

        <div class="card-body">
            <form action="{{ route('carga_contenedor.store') }}" method="POST">
                @csrf

                <!-- Selector de Contenedor -->
                <div class="mb-3">
                    <label for="contenedor" class="form-label">Contenedor</label>
                    <select class="form-select" id="contenedor" name="contenedor" required>
                        @foreach($contenedores as $contenedor)
                            <option value="{{ $contenedor->id }}">{{ $contenedor->identificador }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Selector de Cargamento -->
                <div class="mb-3">
                    <label for="cargamento" class="form-label">Cargamento</label>
                    <select class="form-select" id="cargamento" name="cargamento" required>
                        @foreach($cargamentos as $cargamento)
                            <option value="{{ $cargamento->id }}">{{ $cargamento->nombre }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Toneladas -->
                <div class="mb-3">
                    <label for="cantidad" class="form-label">Toneladas</label>
                    <input type="number" class="form-control" id="cantidad" name="cantidad" required>
                </div>

                <!-- Activo -->
                <div class="mb-3">
                    <label for="activo" class="form-label">Activo</label>
                    <select class="form-select" id="activo" name="activo" required>
                        <option value="1">Sí</option>
                        <option value="0">No</option>
                    </select>
                </div>

                <!-- Origen -->
                <div class="mb-3">
                    <label for="origen" class="form-label">Origen</label>
                    <input type="text" class="form-control" id="origen" name="origen" required>
                </div>

                <!-- Destino -->
                <div class="mb-3">
                    <label for="destino" class="form-label">Destino</label>
                    <input type="text" class="form-control" id="destino" name="destino" required>
                </div>

                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
