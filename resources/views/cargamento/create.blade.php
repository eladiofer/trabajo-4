@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
@include('user-welcome')

            <a href="{{ route('home') }}" class="btn btn-secondary">Volver atrás</a>
        </div>

        <div class="card-body">
            <form action="{{ route('cargamento.store') }}" method="POST">
                @csrf

                <div class="mb-3">
                    <label for="name" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>

                <div class="mb-3">
                    <label for="controlado" class="form-label">Carga controlada</label>
                    <select name="controlado" id="controlado">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
