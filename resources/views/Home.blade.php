@extends('layouts.app')
<div class="container">
    <h1>Eladio Fernandez</h1>
    @include('user-welcome')
    <div class="text-center">
        @if (auth()->check())
            <!-- El usuario está autenticado, mostrar enlace de Cerrar Sesión -->
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger">
                Cerrar Sesión
            </a>
    
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @else
            <!-- El usuario no está autenticado, mostrar enlace de Iniciar Sesión -->
            <a href="{{ route('login') }}" class="btn btn-primary">
                Iniciar Sesión
            </a>
        @endif
    </div>    
    <div class="card">
        <div class="card-header">
            <h2>Conntrol de cargamento</h2>
        </div>
        <div class="card-body">
            <img src="{{ asset('img/DER.jpeg') }}" alt="aca va la imagen del DER" class="img-fluid"
                style="max-width: 100%; height: auto;">
        </div>
    </div>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-4">
                <div class="card bg-light">
                    <div class="card-header bg-primary text-white">
                        <h2>Modulos</h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header bg-success text-white">
                                        <h3>Cargamento</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ route('cargamento.index') }}" class="btn btn-success">Ir al modulo</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        <h3>Contenedor</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ route('contenedor.index') }}" class="btn btn-info text-white">Ir al modulo</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <div class="card-header bg-danger text-white">
                                        <h3>Cargamento por Contenedor</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ route('carga_contenedor.index') }}" class="btn btn-danger">Ir al modulo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    