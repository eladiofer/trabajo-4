@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
@include('user-welcome')

            <a href="{{ route('home') }}" class="btn btn-secondary">Volver atrás</a>
        </div>

        <div class="card-body">
            <form action="{{ route('contenedor.store') }}" method="POST">
                @csrf

                <div class="mb-3">
                    <label for="identificador" class="form-label">Identificador</label>
                    <input type="text" class="form-control" id="identificador" name="identificador" required>
                </div>
                <div class="mb-3">
                    <label for="toneladas" class="form-label">Toneladas Maximas</label>
                    <input type="text" class="form-control" id="toneladas" name="toneladas" required>
                </div>
                <div class="mb-3">
                    <label for="color" class="form-label">Color</label>
                    <input type="text" class="form-control" id="color" name="color" required>
                </div>
                <div class="mb-3">
                    <label for="controlado" class="form-label">activo</label>
                    <select name="activo" id="activo">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection
