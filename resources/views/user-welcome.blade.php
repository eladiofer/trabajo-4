@extends('layouts.app')
@auth
    <span>Bienvenido, {{ auth()->user()->email }}</span>
@else
    <span>No estás logueado en este momento.</span>
@endauth
